# Test FrontEnd

## Project setup
```
npm install
```

### Run json-server
##### - project use json-server to mock api data in page #3
##### - run it to look data show in table.
```
json-server --watch db.json
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Run eslint
```
npm run lint
```

### Login with account
```
username: admin
password: admin
```

### link to page

```
Login: /login
Register: /register
ListUser: /
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```


