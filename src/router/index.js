import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from "../views/Login";
import Register from "../views/Register";
import ListUserViewer from "../views/ListUserViewer";
import axios from 'axios';
import Constants from '@/utils/constants';
import Auth from '@/utils/auth.js';
import PageNotFound from "../views/PageNotFound";

Vue.use(VueRouter)

const routes = [
    {
        path: '/login',
        name: 'Login',
        component: Login,
        meta: {
            title: "Login",
        }
    },
    {
        path: '/register',
        name: 'Register',
        component: Register,
        meta: {
            title: "Register",
        }
    },
    {
        path: '/',
        name: 'ListUserViewer',
        component: ListUserViewer,
        meta: {
            title: "List User",
        },
        beforeEnter: (to, from, next) => {
            if (Auth.getHeader()) {
                axios({
                    method: 'GET',
                    url: `${Constants.SERVER}/users`,
                    headers: Auth.getHeader()
                }).then(function (response) {
                    if (response.status === 200) {
                        next();
                    } else {
                        next({path: "/login"});
                    }
                });
            }
            if (!Auth.getHeader()) {
                next({path: "/login"});
            }
        }
    },
    {
        path: "*",
        component: PageNotFound,
    },

];

const router = new VueRouter({
    mode: "history",
    routes

});

export default router
