import axios from 'axios';
import Constants from '@/utils/constants';

export const userService = {
    login,
    getListUser,
};

function login(username, password) {
    const data = JSON.stringify({
        "username": username,
        "password": password,
    });
    return axios({
        method: 'POST',
        url: `${Constants.SERVER}/login`,
        data: data,
        header: {"content-type": "application/json"},
    });
}

function getListUser(page, limit) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER_DATABASE}/users?_page=${page}&_limit=${limit}`,
        header: {"content-type": "application/json"},
    });
}
