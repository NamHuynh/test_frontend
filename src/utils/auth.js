export default {
    getHeader() {
        let token = localStorage.getItem('token');

        if (token) {
            return { Authorization: 'apikey ' + token };
        } else {
            return null;
        }
    },
}
